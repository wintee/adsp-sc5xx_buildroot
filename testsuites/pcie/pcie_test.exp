#!/usr/bin/expect
#
# Test PCIe host controller driver on target board
#
# Usage:
#   ./pcie_test.exp board_type
#
# Example:
#   ./pcie_test.exp SC58X-EZKIT
#

source ../common/kernel_config.exp
log_file [log_file_name "$argv0"]
send_user "Starting $argv0\n"
set TITLE [title "$argv0"]

step "Start kermit"
source ../common/spawn_kermit.exp

step "Boot kernel"
# Different bootargs can be set here.
# For example:
# set bootargs $bootargs_param1
source ../common/kernel_boot.exp

set timeout 10
expect -re $kernel_prompt
send "\r"
expect -re $kernel_prompt

step "Start testing"
set case_num 0

incr case_num
set timeout 30
send "dmesg | grep -i pci\r"
while 1 {
    expect {
        -re "\[eE]rror|\[fF]ail|Hardware Trace|SIGSEGV" {
            case_fail $case_num
        }
        -re "PCI: bus0.*$kernel_prompt" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 30
send "lspci\r"
while 1 {
    expect {
        -re "\[eE]rror|\[fF]ail|Hardware Trace|SIGSEGV" {
            case_fail $case_num
        }
        -re "Class\[a-z0-9 :].*$kernel_prompt" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

all_pass
send_user "Ending $argv0\n"
log_file
