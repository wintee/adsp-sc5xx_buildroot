#!/bin/bash  
CC=arm-linux-gnueabi-gcc
EXEC=dhrystone
CFLAGS=
tmp=
cpu=
ITT=10000000
IP=$1
DIR=$2
DHRYSTONE_DIR="output/build/dhrystone-2"
cd $DIR
        cpu=`ssh -l root $IP 'cpufreq-info | grep "current CPU frequency"'  | awk  '{print $5}'` ;\
	echo "testing on $cpu MHz with $CC" ;\
	echo "^ Flags ((standard ''CFLAGS'' include ''OPTIMIZATION''))  ^  size (bytes)  ^  Loops  ^  Dhrystones per Second ((Based on $CC)) ^  Dhrystone MIPS ((DMIPS is obtained when the Dhrystone score is divided by 1,757 (the number of Dhrystones per second obtained on the VAX 11/780, nominally a 1 MIPS machine))  ^  DMIPS/MHz  ^";\
	( for level in -O{s,0,1,2,3}{" "," -fomit-frame-pointer"}{" "," -static"}{""," -ffast-math"}{""," -funroll-loops"}{""," -funsafe-loop-optimizations"} ; do \
	    if [ `echo $CC |grep uclibc|wc -l` -eq 1 -a `echo ${level} | grep static | wc -l` -eq 1 ] ; then \
		continue; \
	    fi; \
	    echo -n "| ''${level}'' " | sed 's/ [ ]*/ /g' | sed "s/ '' /''/" ; \
            pwd ; \
	    make  dhrystone-dirclean ; \
	    make  dhrystone TARGET_OPTIMIZATION="$level" -w > /dev/null; \
	    scp $DHRYSTONE_DIR/$EXEC root@$IP:/var/$EXEC > /dev/null 2>&1; \
	    ssh -l root $IP /var/$EXEC $ITT ;\
	  done; \
	)
