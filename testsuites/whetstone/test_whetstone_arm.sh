#!/bin/bash  
CC=arm-linux-gnueabi-gcc
EXEC=whetstone
CFLAGS=
tmp=
cpu=
ITT=10000
IP=$1
DIR=$2
WHETSTONE_DIR="output/build/whetstone-*"
cd $DIR
        cpu=`ssh -l root $IP 'cpufreq-info | grep "current CPU frequency"'  | awk  '{print $5}'` ;\
	echo "testing on $cpu MHz with $CC" ;\

        echo "^ Flags  ^  size  ^  Loops  ^  Duration (seconds) ^  Double Precision Whetstones MIPS  ^"; \
        ( for level in -O{s,0,1,2,3}{" "," -fomit-frame-pointer"}{" "," -ffast-math"} ; do \
            echo -n "| ''${level}'' " | sed 's/ [ ]*/ /g' | sed "s/ '' /''/"; \
	    pwd ; \
	    make  whetstone-dirclean ; \
	    make  whetstone TARGET_OPTIMIZATION="$level" -w > /dev/null; \
	    scp $WHETSTONE_DIR/$EXEC root@$IP:/var/$EXEC > /dev/null 2>&1; \
	    ssh -l root $IP /var/$EXEC $ITT ;\
          done; \
        )

