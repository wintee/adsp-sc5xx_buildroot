#!/bin/sh

rm -rf /lib/modules
rm -rf /lib/libgfortran*
rm -rf /lib/libthread*
rm -rf /lib/libstdc*

SUBDIR=`ls / | grep -v mnt | grep -v proc | grep -v sys | grep -v usr | grep -v tmp`

for i in $SUBDIR
do
	cp -ar $i /mnt/rootfs
	echo "copy $i finish"
done

mkdir /mnt/rootfs/mnt
mkdir /mnt/rootfs/proc
mkdir /mnt/rootfs/tmp
mkdir /mnt/rootfs/sys
mkdir /mnt/rootfs/usr
mkdir /mnt/rootfs/usr/libexec
mkdir /mnt/rootfs/usr/bin
mkdir /mnt/rootfs/usr/lib

test -e /usr/libexec/rshd && cp /usr/libexec/rshd /mnt/rootfs/usr/libexec
test -e /usr/bin/rcp && cp /usr/bin/rcp /mnt/rootfs/usr/bin
test -e /usr/bin/rsh && cp /usr/bin/rsh /mnt/rootfs/usr/bin
test -e /usr/lib/libtirpc.so && cp /usr/lib/libtirpc* /mnt/rootfs/usr/lib

test -e /usr/libexec/rshd && chmod 777 /usr/libexec/rshd /usr/bin/rcp /usr/bin/rsh

echo "copy rootfs done"
