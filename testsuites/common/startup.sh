#!/bin/bash -x

export BOARD_CONFIG=$NODE_NAME
export custom_workspace=$WORKSPACE
export WORKSPACE_ROOT=/home/test/workspace
export TERM=xterm
use_local_src_server=1
SERVER_ADDR=10.99.29.20
TEST_FRAME_REPO_ADDR=git://$SERVER_ADDR/git/unreleased/buildroot-testsuites
KERNEL_REPO_ADDR=git://$SERVER_ADDR/git/unreleased/linux-kernel

# Set repository info
MAIN_PROJ_REPO_NAME=buildroot
TEST_FRAME_REPO_NAME=testsuites
KERNEL_REPO_NAME=kernel

export cces_version=2.6.0
export linux_addin_version=1.2.0
export board_name=sc58x
export pkg_install_dir=cces-linux-add-in

MAIN_PROJ_INDEX=develop-linuxaddin-1.2.0
TEST_FRAME_INDEX=develop-linuxaddin-1.2.0
KERNEL_INDEX=develop-linuxaddin-1.2.0


# check if use right source
debug=0

check_git_info ()
{
    echo "##########"
    if [ -d $custom_workspace ] ; then
        cd $custom_workspace
        git branch
        git remote -v
    fi


    if [ -d $custom_workspace/testsuites ] ; then
        cd $custom_workspace/testsuites
        git branch
        git remote -v
    fi

    if [ -d $custom_workspace/linux/linux-kernel ] ; then
        cd $custom_workspace/linux/linux-kernel
        git branch
        git remote -v
    fi
    echo "##########"
}

if [ $debug -eq 1 ] ; then
    check_git_info
fi


# Checkout master project, switch to desired branch and get the latest source.
cd $custom_workspace
if [ "`git branch | grep -c \"$MAIN_PROJ_INDEX\"`" -eq 0 ] ; then
    git checkout -b $MAIN_PROJ_INDEX remotes/$MAIN_PROJ_REPO_NAME/$MAIN_PROJ_INDEX
else
    git checkout $MAIN_PROJ_INDEX
fi

git pull $MAIN_PROJ_REPO_NAME $MAIN_PROJ_INDEX


# Use local submodule git server
cd $custom_workspace
git submodule init
if [ $use_local_src_server -eq 1 ] ; then
    git config submodule.testsuites.url $TEST_FRAME_REPO_ADDR
    git config submodule.linux/linux-kernel.url $KERNEL_REPO_ADDR
fi
git submodule update


if [ $debug -eq 1 ] ; then
    check_git_info
fi


# Checkout test frame and switch to desired branch.
cd $custom_workspace/testsuites
if [ "`git branch | grep -c \"$TEST_FRAME_INDEX\"`" -eq 0 ] ; then
    git checkout -b $TEST_FRAME_INDEX remotes/origin/$TEST_FRAME_INDEX
else
    git checkout $TEST_FRAME_INDEX
fi


# Checkout kernel and switch to desired branch.
cd $custom_workspace/linux/linux-kernel
#git checkout .
if [ "`git branch | grep -c \"$KERNEL_INDEX\"`" -eq 0 ] ; then
    git checkout -b $KERNEL_INDEX remotes/origin/$KERNEL_INDEX
else
    git checkout $KERNEL_INDEX
fi


# Get the latest source for all submodules.
cd $custom_workspace/testsuites
git pull
cd $custom_workspace/linux/linux-kernel
git pull

if [ $debug -eq 1 ] ; then
    check_git_info
fi

### update startup.sh if changed automatically
#cp $custom_workspace/testsuites/common/startup.sh /home/test

### check out the latest deb kit
if [ ! -d $WORKSPACE_ROOT/../kitmirror ] ; then
    mkdir -p $WORKSPACE_ROOT/../kitmirror
fi

if [ ! -d $WORKSPACE_ROOT/../debian_install ] ; then
    mkdir $WORKSPACE_ROOT/../debian_install
fi

if [ ! -d $WORKSPACE_ROOT/../debian_install/installer ] ; then
   cd $WORKSPACE_ROOT/../debian_install	
   git clone https://testlab2:Labrat1@gitlab.analog.com/dte-test/installer.git
fi

cd $WORKSPACE_ROOT/../debian_install/installer
git pull
git log -1

cp $custom_workspace/testsuites/common/debian_install.exp $WORKSPACE_ROOT/..
cd $WORKSPACE_ROOT/..
./debian_install.exp


export install_path=/opt/analog
export PATH=$install_path/cces/$cces_version/ARM/arm-none-eabi/bin:$install_path/$pkg_install_dir/$linux_addin_version/ARM/arm-linux-gnueabi/bin:$PATH

echo "PATH is $PATH !!"

#############################################################
#############################################################
### To decide if we run kernel test OR just build image ##### 

cd $custom_workspace/testsuites/common/

if [  $# -lt 1 ] ; then

  # Start regression test
  
  ./run_kernel_test

else
  ./make_buildroot_package

fi
