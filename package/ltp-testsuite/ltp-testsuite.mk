################################################################################
#
# ltp-testsuite
#
################################################################################

LTP_TESTSUITE_VERSION = 20140828
LTP_TESTSUITE_SOURCE  = ltp-full-$(LTP_TESTSUITE_VERSION).tar.xz
LTP_TESTSUITE_SITE    = http://downloads.sourceforge.net/project/ltp/LTP%20Source/ltp-$(LTP_TESTSUITE_VERSION)
LTP_TESTSUITE_LICENSE = GPLv2 GPLv2+
LTP_TESTSUITE_LICENSE_FILES = COPYING

# Needs libcap with file attrs which needs attr, so both required
ifeq ($(BR2_PACKAGE_LIBCAP)$(BR2_PACKAGE_ATTR),yy)
LTP_TESTSUITE_DEPENDENCIES += libcap
else
LTP_TESTSUITE_CONF_ENV += ac_cv_lib_cap_cap_compare=no
endif

LTP_TESTSUITE_CFLAGS = $(TARGET_CFLAGS)
LTP_TESTSUITE_LDFLAGS = $(TARGET_LDFLAGS)

ifeq ($(BR2_PACKAGE_LIBTIRPC),y)
LTP_TESTSUITE_DEPENDENCIES += libtirpc
LTP_TESTSUITE_CFLAGS += -I$(STAGING_DIR)/usr/include/tirpc
LTP_TESTSUITE_LDFLAGS += -L$(STAGING_DIR)/usr/lib  -ltirpc
endif

# ltp-testsuite uses <fts.h>, which isn't compatible with largefile
# support.
LTP_TESTSUITE_CONF_ENV += \
	CPPFLAGS="$(filter-out -D_FILE_OFFSET_BITS=64,$(TARGET_CPPFLAGS))" \
	CFLAGS="$(filter-out -D_FILE_OFFSET_BITS=64,$(LTP_TESTSUITE_CFLAGS))" \
	LDFLAGS="$(LTP_TESTSUITE_LDFLAGS)" 

$(BUILD_DIR)/ltp-testsuite-$(LTP_TESTSUITE_VERSION)/.stamp_extracted:
	@$(call MESSAGE,"Extracting")
	$($(PKG)_EXTRACT_CMDS)
	rm -rf $(@D)/testcases/network/rpc/rpc-tirpc/tests_pack/rpc_suite/tirpc/tirpc_auth_authdes_*

$(eval $(autotools-package))
