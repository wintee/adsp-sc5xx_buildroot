#############################################################
#
# ad7928 SPI ADC test application
#
#############################################################
AD7928_SPI_TEST_SITE = http://svn.code.sf.net/p/adi-openapp/code/trunk/tests/ad7928_test
AD7928_SPI_TEST_SITE_METHOD = svn
AD7928_SPI_TEST_VERSION = HEAD

define AD7928_SPI_TEST_BUILD_CMDS
	$(MAKE) -C $(@D) $(TARGET_CONFIGURE_OPTS)
endef

define AD7928_SPI_TEST_INSTALL_TARGET_CMDS
	if ! [ -d "$(TARGET_DIR)/bin/" ]; then \
		mkdir -p $(TARGET_DIR)/bin/; \
	fi
	$(INSTALL) -D -m 0755 $(@D)/ad7928_spi_test $(TARGET_DIR)/bin/
endef

define AD7928_SPI_TEST_CLEAN_CMDS
	$(MAKE) -C $(@D) clean
endef

define AD7928_SPI_TEST_UNINSTALL_TARGET_CMDS
	rm -f $(TARGET_DIR)/bin/ad7928_spi_test
endef

$(eval $(generic-package))
