#############################################################
#
# pound
#
#############################################################

POUND_VERSION = 2.7
POUND_SITE = http://www.apsis.ch/pound/
POUND_SOURCE = Pound-$(POUND_VERSION).tgz
POUND_DEPENDENCIES = openssl
POUND_CONF_OPTS = LIBS=-lz

#define POUND_CONFIGURE_CMDS
#	$(CONFIG_UPDATE) $(@D)
#	sed -i 's/CFLAGS=/CFLAGS+=/g' $(@D)/Makefile
#	sed -i '/cd .*doc/d' $(@D)/Makefile
#	sed -i '/include/d' $(@D)/Makefile
#	touch $@
#endef
#F_CONF="pound.cfg"
POUND_CFLAGS = $(TARGET_CFLAGS) -I$(LINUX_SRCDIR)/include -DHAVE_SYSLOG_H=1 -DVERSION=$(POUND_VERSION) -pthread -DNEED_STACK -DEMBED -D_REENTRANT -D_THREAD_SAFE -DUPER

define POUND_BUILD_CMDS
	$(MAKE) CFLAGS="$(POUND_CFLAGS)" OS=$(ARCH) CC="$(TARGET_CC)" -C $(@D)
endef

define POUND_INSTALL_TARGET_CMDS
        cp -a $(@D)/pound $(TARGET_DIR)/bin/pound
	test -d $(TARGET_DIR)/etc/pound || mkdir $(TARGET_DIR)/etc/pound
	cp package/pound/mycert.pem $(TARGET_DIR)/etc/pound
	test -d $(TARGET_DIR)/usr/local	|| (mkdir -p $(TARGET_DIR)/usr/local/etc)
	cp package/pound/pound.cfg $(TARGET_DIR)/usr/local/etc

endef

define POUND_CLEAN_CMDS
	$(MAKE) -C $(@D) clean
endef

$(eval $(autotools-package))

