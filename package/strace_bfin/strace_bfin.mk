################################################################################
#
# strace for blackfin
#
################################################################################

STRACE_BFIN_VERSION = HEAD
STRACE_BFIN_SITE_METHOD = svn
STRACE_BFIN_SITE = http://svn.code.sf.net/p/adi-openapp/code/trunk/apps/strace
STRACE_BFIN_FLAT_STACKSIZE=0x5000

STRACE_BFIN_CONF_ENV = ac_cv_header_linux_if_packet_h=yes \
		  ac_cv_header_linux_netlink_h=yes \
		  ac_cv_have_long_long_off_t=no \
		  $(if $(BR2_LARGEFILE),ac_cv_type_stat64=yes,ac_cv_type_stat64=no)

STRACE_BFIN_SUBDIR = strace-4.6

define STRACE_BFIN_CHECK_ENTITIES 
	cd $(@D) && \
	./check-entities.sh $(TOPDIR)/$(LINUX_SRCDIR) $(KERNEL_ARCH) $(STRACE_BFIN_SUBDIR) $(ARCH)
endef
STRACE_BFIN_PRE_CONFIGURE_HOOKS += STRACE_BFIN_CHECK_ENTITIES

define STRACE_BFIN_REMOVE_STRACE_GRAPH
	rm -f $(TARGET_DIR)/usr/bin/strace-graph
endef

STRACE_BFIN_POST_INSTALL_TARGET_HOOKS += STRACE_BFIN_REMOVE_STRACE_GRAPH

$(eval $(autotools-package))
