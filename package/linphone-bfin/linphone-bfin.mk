################################################################################
#
# linphone
#
################################################################################

LINPHONE_BFIN_VERSION = 3.3.2
LINPHONE_BFIN_SOURCE = linphone-3.3.2.tar.gz
LINPHONE_BFIN_SITE = http://download-mirror.savannah.gnu.org/releases/linphone/3.3.x/sources/

LINPHONE_BFIN_CONF_OPTS += \
	--without-crypto \
	--enable-portaudio=no \
	--enable-gtk_ui=no \
	--disable-manual \
	--disable-strict \
	--disable-glib \
	--disable-video \
	--enable-ipv6=no \
	--disable-shared \
	--enable-static \
	--with-thread-stack-size=0xa000

# configure is out of sync causing deplibs linking issues
LINPHONE_BFIN_AUTORECONF = YES
LINPHONE_BFIN_DEPENDENCIES = host-pkgconf ortp mediastreamer libeXosip2 speex
LINPHONE_BFIN_LICENSE = GPLv2+
LINPHONE_BFIN_LICENSE_FILES = COPYING

LINPHONE_BFIN_FLAT_STACKSIZE=0x40000
LINPHONE_BFIN_CONF_ENV = CFLAGS="$(TARGET_CFLAGS) -fno-strict-aliasing -ffast-math -mfast-fp"
LINPHONE_BFIN_DEPENDENCIES += libbfgdots

ifeq ($(BR2_PACKAGE_LIBGTK2)$(BR2_PACKAGE_XORG7),yy)
LINPHONE_CONF_OPTS += --enable-gtk_ui
LINPHONE_DEPENDENCIES += libgtk2
else
LINPHONE_CONF_OPTS += --disable-gtk_ui
endif

$(eval $(autotools-package))
