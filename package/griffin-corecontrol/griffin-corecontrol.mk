#############################################################
#
# bfin linkport test
#
#############################################################
GRIFFIN_CORECONTROL_SOURCE =

GRIFFIN_CORECONTROL_SRC:=corecontrol.c
GRIFFIN_CORECONTROL_EXE:=corecontrol

GRIFFIN_CORECONTROL_CFLAGS = $(TARGET_CFLAGS) -I$(LINUX_SRCDIR)/drivers/staging/icc/include

define GRIFFIN_CORECONTROL_BUILD_CMDS
        $(TARGET_CC) $(GRIFFIN_CORECONTROL_CFLAGS) $(TARGET_LDFLAGS) \
	      $(TOPDIR)/package/griffin-corecontrol/$(GRIFFIN_CORECONTROL_SRC) -o $(@D)/$(GRIFFIN_CORECONTROL_EXE)
endef

define GRIFFIN_CORECONTROL_INSTALL_TARGET_CMDS
	$(INSTALL) -m 0755 -D $(@D)/$(GRIFFIN_CORECONTROL_EXE) $(TARGET_DIR)/bin/
endef

define GRIFFIN_CORECONTROL_CLEAN_CMDS
	$(MAKE) -C $(@D) clean
endef

define GRIFFIN_CORECONTROL_UNINSTALL_TARGET_CMDS
	rm -f $(TARGET_DIR)/bin/$(GRIFFIN_CORECONTROL_EXE)
endef

$(eval $(generic-package))
